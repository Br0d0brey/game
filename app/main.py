from fastapi import FastAPI, HTTPException, Request, status
from fastapi.responses import JSONResponse
from .map_constructor import Map, Object, Cube
import json
import os
import asyncio
from dotenv import load_dotenv
from data_base.tables import Database
import logging


app = FastAPI()
database = Database()
load_dotenv()
logger = logging.getLogger(__name__)


@app.on_event("startup")
async def startup_db_client():
    # await asyncio.sleep(10)
    app.db_connection = await database.get_database_connection()
    await database.migrate(app.db_connection)

@app.on_event("shutdown")
async def shutdown_db_client():
    await app.db_connection.close()



@app.post("/create_map/")
async def create_map(request: Request):
    request_body = await request.body()
    json_data = json.loads(request_body.decode())
    name = json_data['name']
    width = json_data['width']
    height = json_data['height']
    length = json_data['length']
    map_instance = Map(database=database, name=name, width=width, height=height, length=length)
    await map_instance.save()
    return JSONResponse(content={}, status_code=201)


@app.post("/create_object/")
async def create_object(request: Request):
    request_body = await request.body()
    json_data = json.loads(request_body.decode())
    map_id = json_data['map_id']
    x = json_data['x']
    y = json_data['y']
    z = json_data['z']
    object_type = json_data['object_type']
    destruct_ct = json_data['destruct_ct']
    variables = [[
        [0, 0, 0],
        [1, 0, 0],
        [0, 1, 1],
        [1, 1, 1]
    ]]
    object = Object(database, object_type, x, y, z, map_id)
    await object.save()
    for variable in variables:
        cube = Cube(database, object.id, variable, destruct_ct)
        await cube.save()
        await object.add_cube(cube)
    return JSONResponse(content={}, status_code=201)

@app.get("/maps/{id}")
async def get_objects(request: Request, id: int):
    connection = await database.get_database_connection()
    try:
        async with connection.cursor() as cursor:
            query = """
                SELECT 
                    o.id as object_id,
                    o.object_type,
                    o.x,
                    o.y,
                    o.z,
                    c.id as cube_id,
                    c.destruct_ct,
                    c.variables
                FROM objects o
                LEFT JOIN cubes c ON o.id = c.object_id
                WHERE o.map_id = %s
            """
            await cursor.execute(query, (id,))
            result = await cursor.fetchall()

            data = []
            current_object_id = None
            current_object = None

            for row in result:
                if row[0] != current_object_id:
                    current_object = {
                        'id': row[0],
                        'object_type': row[1],
                        'x': row[2],
                        'y': row[3],
                        'z': row[4],
                        'cubes': [],
                    }
                    data.append(current_object)
                    current_object_id = row[0]

                if row[5] is not None:
                    cube_data = {
                        'id': row[5],
                        'destruct_ct': row[6],
                        'variables': row[7],
                    }
                    current_object['cubes'].append(cube_data)

            if data:
                return JSONResponse(content=data, status_code=200)
            else:
                return JSONResponse(content={}, status_code=404)
    except Exception as e:
        print(f"Error executing query: {e}")
    finally:
        await connection.close()

@app.get("/maps/")
async def get_maps(request: Request):
    connection = await database.get_database_connection()
    async with connection.cursor() as cursor:
        query = "SELECT * FROM maps"
        await cursor.execute(query)
        result = await cursor.fetchall()
        return JSONResponse(result)