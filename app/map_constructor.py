import json
import logging

logger = logging.getLogger(__name__)

class Map:
    objects = []

    def __init__(self, database, name: str, width: float, height: float, length: float):
        self.id = 0
        self.database = database
        self.name = name
        self.width = width
        self.height = height
        self.length = length

    async def save(self, id=None):
        connection = await self.database.get_database_connection()
        if id:
            query = ''
        else:
            query = """
                        INSERT INTO maps (name, width, height, length)
                        VALUES (%s, %s, %s, %s)
                        RETURNING id
                    """
        try:
            async with connection.cursor() as cursor:
                await cursor.execute(query, ([self.name, self.width, self.height, self.length]))
                result = await cursor.fetchone()
                if result:
                    self.id = result[0]
        except Exception as e:
            print(f"Error executing query: {e}")
        finally:
            await connection.close()

    async def add_object(self, obj):
        if await self.check_collision(obj) and await self.check_within_bounds(obj):
            self.objects.append(obj)
            return True
        else:
            return False

    async def check_collision(self, obj):
        for existing_obj in self.objects:
            if await self.detect_collision(obj.vertices, existing_obj.vertices):
                return False
        return True

    async def detect_collision(self, vertices1, vertices2):
        for vertex in vertices1:
            if vertex in vertices2:
                return True
        return False

    async def check_within_bounds(self, obj):
        for vertex in obj.vertices:
            x, y, z = vertex
            if not (0 <= x <= self.width and 0 <= y <= self.height and 0 <= z <= self.length):
                return False
        return True

    async def delete_object(self, id):
        for object in self.objects:
            if object.id == id:
                self.objects.remove(object)
                break

    async def info(self):
        return self.objects


class Cube:
    def __init__(self, database, object_id: int, variables: list, destruct_ct: float):
        self.id = 0
        self.database = database
        self.variables = json.dumps(variables)
        self.destruct_ct = destruct_ct
        self.object_id = object_id

    async def save(self, id=None):
        connection = await self.database.get_database_connection()
        if id:
            query = ''
        else:
            query = """
                        INSERT INTO cubes (object_id, destruct_ct, variables)
                        VALUES (%s, %s, %s)
                        RETURNING id
                    """
        try:
            async with connection.cursor() as cursor:
                await cursor.execute(query, ([self.object_id, self.destruct_ct, self.variables]))
                result = await cursor.fetchone()
                if result:
                    self.id = result[0]
        except Exception as e:
            print(f"Error executing query: {e}")
        finally:
            await connection.close()


class Object:
    cubes = []

    def __init__(self, database, object_type: str, x: float, y: float, z: float, map_id: int):
        self.id = 0
        self.database = database
        self.object_type = object_type
        self.map_id = map_id

        self.x = x
        self.y = y
        self.z = z

    async def add_cube(self, cube):
        self.cubes.append(cube)

    async def save(self, id=None):
        connection = await self.database.get_database_connection()
        if id:
            query = ''
        else:
            query = """
                        INSERT INTO objects (object_type, x, y, z, map_id)
                        VALUES (%s, %s, %s, %s, %s)
                        RETURNING id
                    """
        try:
            async with connection.cursor() as cursor:
                await cursor.execute(query, ([self.object_type, self.x, self.y, self.z, self.map_id]))
                result = await cursor.fetchone()
                if result:
                    self.id = result[0]

        except Exception as e:
            print(f"Error executing query: {e}")
        finally:
            await connection.close()
