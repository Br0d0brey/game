import os
import aiopg
import asyncio

class Database:
    async def get_database_connection(self):
        connection = await aiopg.connect(
            host=os.getenv("DB_HOST"),
            port=os.getenv("DB_INNER_PORT"),
            user=os.getenv("DB_USER"),
            password=os.getenv("DB_PASSWORD"),
            database=os.getenv("DB_NAME")
        )
        return connection

    async def migrate(self, connection):
        await self.create_maps(connection)
        await self.create_objects(connection)
        await self.create_cubes(connection)

    async def table_exists(self, connection, table_name):
        async with connection.cursor() as cursor:
            await cursor.execute(
                f"SELECT EXISTS (SELECT 1 FROM information_schema.tables WHERE table_name = '{table_name}')"
            )
            result = await cursor.fetchone()
            return result[0]

    async def create_maps(self, connection):
        if not await self.table_exists(connection, "maps"):
            async with connection.cursor() as cursor:
                await cursor.execute(
                    """
                    CREATE TABLE maps (
                        id SERIAL PRIMARY KEY,
                        name VARCHAR(255) NOT NULL,
                        width DOUBLE PRECISION NOT NULL,
                        height DOUBLE PRECISION NOT NULL,
                        length DOUBLE PRECISION NOT NULL
                    )
                    """
                )

    async def create_objects(self, connection):
        if not await self.table_exists(connection, "objects"):
            async with connection.cursor() as cursor:
                await cursor.execute(
                    """
                    CREATE TABLE objects (
                        id SERIAL PRIMARY KEY,
                        object_type VARCHAR(255) NOT NULL,
                        x DOUBLE PRECISION NOT NULL,
                        y DOUBLE PRECISION NOT NULL,
                        z DOUBLE PRECISION NOT NULL,
                        map_id INTEGER REFERENCES maps(id) ON DELETE CASCADE
                    )
                    """
                )

    async def create_cubes(self, connection):
        if not await self.table_exists(connection, "cubes"):
            async with connection.cursor() as cursor:
                await cursor.execute(
                    """
                    CREATE TABLE cubes (
                        id SERIAL PRIMARY KEY,
                        object_id INTEGER REFERENCES objects(id) ON DELETE CASCADE,
                        destruct_ct DOUBLE PRECISION NOT NULL,
                        variables JSONB
                    )
                    """
                )